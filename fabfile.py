from fabric.api import run, env
import re
from fabric.contrib import django
from lib.utils import getBytes, get_local_ip
from pprint import pprint
import logging

django.project('monitor')

local_ip = get_local_ip()

if local_ip.startswith('192.') or local_ip.startswith('127.'):
    django.settings_module('monitor.dev_settings')

from apps.stats.models import Mem, Hosts, Load, IO, Net


class Cmd():
    def __init__(self, **kwargs):

        fields = ['cmd', 'regex', 'res', 'hostname',
                  'func', 'default', 'cls', 'unit']

        for field in fields:
            setattr(self, field, kwargs.get(field))

        self.res = self.res or run(kwargs.get('cmd'))
        self.hostname = self.hostname or run('hostname')
        self.func = self.func or str
        self.default = kwargs.get('default') or ''

    def getNames(self):
        name_regex = re.compile(r'\(\?P<(\w+)>')
        return name_regex.findall(self.regex)

    def v(self):
        regex = re.compile(self.regex)
        match = regex.search(self.res)

        names = self.getNames()

        if not match:
            return {}

        ret = {
            'hostname': self.hostname,
        }

        for i in names:
            ret[i] = match.group(i)
            if self.func:
                try:
                    ret[i] = self.func(ret[i] or self.default)
                except Exception as e:
                    print 'error func', e

        return ret

    def reg(self):
        v = self.v()
        s = self.cls()
        pprint(v, indent=2)
        s.set(**v)


def mem():

    hostname = run('hostname')
    cmd = '`which top` | head -5'
    regex = r'''(?imx)
                 (?P<active>\d+[BKMGTPEZY])\s+Active[,\s]*
                 (?P<inact>\d+[BKMGTPEZY])\s+Inact[,\s]*
                 (?P<wired>\d+[BKMGTPEZY])\s+Wired[,\s]*
                 (?:(?P<cache>\d+[BKMGTPEZY])\s+Cache[,\s]*)?
                 (?P<buf>\d+[BKMGTPEZY])\s+Buf[,\s]*
                 (?P<free>\d+[BKMGTPEZY])\s+Free[,\s]*'''

    res = run(cmd)

    Cmd(
        regex=regex,
        res=res,
        cls=Mem,
        func=getBytes,
        hostname=hostname,
    ).reg()

    regex = r"""(?xi)\bload\s+averages:\s+
                (?P<load_1>[\d.]+),\s+
                (?P<load_5>[\d.]+),\s+
                (?P<load_15>[\d.]+)"""

    Cmd(regex=regex,
        res=res,
        func=float,
        default=-1,
        hostname=hostname,
        cls=Load,).reg()


def uptime():

    cmd = '''a=`date +%s`;b=`sysctl kern.boottime |awk '{print $5}' |
           tr -d ','`;echo $((a-b)) '''
    regex = r'''(?x) (?P<uptime>\d+) '''

    Cmd(
        cmd=cmd,
        regex=regex,
        cls=Hosts,
        func=int,
        default=0).reg()


def misc():

    hostname = run('hostname')

    cmd = '''ifconfig |grep inet |grep -v 127.0.0.1 |grep netmask |awk '{print $2}' '''
    regex = r'(?P<ip>[\d.]+)'
    Cmd(
        regex=regex,
        cmd=cmd,
        cls=Hosts,
        hostname=hostname,
    ).reg()

    #disk
    cmd = '''/bin/df -l -P -c |grep '^total' '''
    regex = r'(?P<disk>\d+(?=%))'
    Cmd(
        regex=regex,
        cmd=cmd,
        cls=Hosts,
        hostname=hostname,
    ).reg()


def io():
    """
           r/s     read operations per second
           w/s     write operations per second
           kr/s    kilobytes read per second
           kw/s    kilobytes write per second
           wait    transactions queue length
           svc_t   average duration of transactions, in milliseconds
           %b      % of time the device had one or more outstanding transactions
    """

    cmd = '''iostat -d -x -K 1 2 |grep da0 |tail -1'''
    res = run(cmd).split()[1:]
    try:
        r_s, w_s, kr_s, kw_s, wait, svc_t, pct_b = res
        IO.set(
            r_s=r_s,
            w_s=w_s,
            kr_s=kr_s,
            kw_s=kw_s,
            wait=wait,
            svc_t=svc_t,
            pct_b=pct_b,
            hostname=run('hostname'),
        )
        logging.info('io stats saved')
    except ValueError as e:
        logging.error('Saving IO Error: %r' % e)


def disk_total():
    pass


def disk():

    cmd = '''/bin/df -l -P -c |grep -v devfs |grep -v procfs '''


def net():

    cmd = r''' vnstat -tr |grep '\b[rt]x\b' '''
    regex = r'''(?xis)
                 rx\s+ (?P<in_bits_per_s>[\d.]+\s*.).*?(?P<in_pks_per_s>\d+)(?=\s+packets/s).*
                 tx\s+ (?P<out_bits_per_s>[\d.]+\s*.).*?(?P<out_pks_per_s>\d+)(?=\s+packets/s)
            '''
    ret = Cmd(
        regex=regex,
        cmd=cmd,
    ).v()

    ret['out_bits_per_s'] = getBytes(ret.get('out_bits_per_s'), float)
    ret['in_bits_per_s'] = getBytes(ret.get('in_bits_per_s'), float)
    Net().set(**ret)