import re
import logging
import prettytable

lg = logging.info
wn = logging.warning
er = logging.error


def getBytes(exp, func=int):
    """
    return human-readable value to Bytes.
     1K = 1024Bytes
     1M = 1024K = 1024*1024 Bytes
     ...
    """
    units = ['B', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y']
    regex = re.compile(r'(?i)(?P<num>[\d.]+)\s*(?P<unit>[%s])' % ''.join(units))
    match = regex.search(exp)
    if not match:
        return 0
    num = func(match.group('num') or 0)
    unit = (match.group('unit') or '').upper()
    if unit not in units:
        return 0
    return int(1024 ** units.index(unit) * num)


def get_value_ignorecase(dictobj, key):
    for k in dictobj.keys():
        if k.lower() == key.lower():
            return dictobj[k]
    return None


def get_attribute_ignorecase(obj, key):
    for a in obj.__dict__:
        if a.lower() == key().lower():
            return getattr(obj, a, None)
    return None


def print_list(data, field, fields=None, formatters={}, html=True):
    if isinstance(data, list):
        objs = data
        if len(objs) > 1:
            title = 'Total: %d' % len(objs)
        else:
            title = None
    elif isinstance(data, dict):
        objs = data.get(field, [])
        if not isinstance(objs, list):
            objs = [objs]
        total = int(data.get('Total', len(objs)))
        limit = int(data.get('Limit', 0))
        offset = int(data.get('Offset', 0))
        if limit > 0:
            pages = int(total)/limit
            if pages*limit < total:
                pages += 1
            page = (offset/limit) + 1
            title = 'Total: %d Pages: %d Limit: %d Offset: %d Page: %d' % \
                    (int(total), pages, limit, offset, page)
        else:
            title = 'Total: %d' % len(objs)
    else:
        objs = []
        title = 'Total: 0'
    if fields is None or len(fields) == 0:
        fields = []
        for o in objs:
            for k in o.keys():
                k = k.upper()
                if k not in fields:
                    fields.append(k)
    pt = prettytable.PrettyTable(fields, caching=False)
    pt.align = 'l'
    data_fields_tbl = {}
    for o in objs:
        row = []
        for field in fields:
            if field in formatters:
                row.append(formatters[field](o))
            else:
                field_name = field.lower().replace(' ', '_')
                if isinstance(o, dict):
                    data = get_value_ignorecase(o, field_name)
                else:
                    data = get_attribute_ignorecase(o, field_name)
                if data is None:
                    data = ''
                elif field not in data_fields_tbl:
                    data_fields_tbl[field] = True
                row.append(data)
        pt.add_row(row)

    data_fields = [f for f in fields if f in data_fields_tbl]

    if html:
        return pt.get_html_string(fields=data_fields) #sortby=fields[0])
    else:
        return pt.get_string(fields=data_fields) #sortby=fields[0])


def print_dict(d, key=None, html=True):
    pt = prettytable.PrettyTable(['Property', 'Value'], caching=False)
    pt.aligns = ['l', 'l']
    if not isinstance(d, dict):
        if key is not None:
            d = getattr(d, key, {})
        dd = {}
        for k in d.__dict__.keys():
            if k[0] != '_':
                v = getattr(d, k)
                if not callable(v):
                    dd[k] = v
        d = dd
    else:
        if key is not None:
            d = d.get(key, {})
    for r in d.iteritems():
        row = list(r)
        pt.add_row(row)

    if html:
        return pt.get_html_string(sortby='Property')
    else:
        return pt.get_string(sortby='Property')

hostname = ''
hostname_abbr = ''


def get_hostname():

    global hostname
    if not hostname:
        hostname = socket.gethostname()
    return hostname


def get_hostname_abbr():

    global hostname_abbr

    if not hostname_abbr:
        hostname = get_hostname() or ''
        hostname_abbr = hostname.split('.')[0][-3:]

    return hostname_abbr


def get_local_ip():
    import socket
    return socket.gethostbyname(socket.gethostname())