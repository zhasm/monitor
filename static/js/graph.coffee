area_graph = (id, yLabelFormat=(x)->x.toString())->
    target = $("##{id}")
    console.log target.html()
    data = JSON.parse target.attr 'data'
    labels = JSON.parse target.attr "keys"
    console.log 'labels', labels
    Morris.Area
        element: id
        data: data
        xkey: "time"
        ykeys: labels
        labels: labels
        yLabelFormat: yLabelFormat

area_graph 'cpu'
area_graph 'mem', digit_byte_formater
area_graph 'io'
area_graph 'net'

$('#host').change ->
    host = $(@).val()
    window.location.href = "/node/#{host}"