$('#io-pop').popover(
    trigger: 'hover'
    html: true
    placement: 'bottom'
    content: """
        <dl>
        <dt>r/s</dt>
        <dd>-      read operations per second
        <dt>w/s   </dt>
        <dd>-   write operations per second
        <dt>kr/s  </dt>
        <dd>-   kilobytes read per second
        <dt>kw/s  </dt>
        <dd>-   kilobytes write per second
        <dt>wait  </dt>
        <dd>-   transactions queue length
        <dt>svc_t </dt>
        <dd>-   average duration of transactions, in milliseconds
        <dt>%b    </dt>
        <dd>-   % of time the device had one or more outstanding transactions
        </dl>
    """
)


$('.hostname').click ->
    #$('tr:invisible').show()
    host = $(@).html()

