# -*- encoding:  utf-8 -*-

from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.html import strip_tags
from django.utils.tzinfo import LocalTimezone
from jingo import register, env
from jinja2 import Markup
import datetime
import hashlib
import jingo
import jinja2
import json
import logging
import re
import time

#logging = logging.getLogger(settings.APPNAME)

@register.function
def url(viewname, *args, **kwargs):
    """Helper for django's reverse in templates."""
    return reverse(viewname, args=args, kwargs=kwargs)

__static_hashes = {}


def abs_path(*args):
    import os
    return os.path.join(settings.ROOT, *args)


def __static_url(path):
    static_url = '%s%s' % (settings.MEDIA_URL, path)

    if not (path in __static_hashes and __static_hashes[path]):
        try:
            f = open(abs_path(settings.MEDIA_ROOT, path))
            __static_hashes[path] = hashlib.md5(f.read()).hexdigest()
            f.close()
        except Exception as e:
            logging.error('path not good ' + str(path) + ' ' + str(e))
            __static_hashes[path] = None

    if __static_hashes[path]:
        if not settings.DEBUG:
            static_url = '%s?v=%s' % (static_url, __static_hashes[path][:5])

    return static_url


@register.function
def static_css(path, media="screen"):
    return Markup("""<link rel="stylesheet" media="%s" href="%s" />""" % (media, __static_url(path)))


@register.function
def static_js(path):
    return Markup("""<script src="%s" type="text/javascript"></script>""" % (__static_url(path),))


@register.function
def static_url(path):
    return __static_url(path)


@register.function
def plain_datetime(d, mini=False):
    if not mini:
        return d.strftime('%Y-%m-%d %H:%M:%S')

    return d.strftime('%Y-%m-%d')


@register.function
def unix_timestamp(d):
    return int(time.mktime(d.timetuple()) * 1000)


@register.function
def display_time(d):
    return d.strftime("%H:%M:%S")


@register.function
def display_datetime(d, mini=False):
    t = time.localtime()
    if d.tzinfo:
        tz = LocalTimezone()
    else:
        tz = None

    now = datetime.datetime(t[0], t[1], t[2], t[3], t[4], t[5], tzinfo=tz)

    if d.year == now.year and d.month == now.month and d.day == now.day:
        return display_time(d)

    return plain_datetime(d, mini)


@register.function
def load_json(s):
    return json.loads(s)


@register.function
def dump_json(o):
    return json.dumps(o)


@register.filter
def strip_and_trunk(text, length):
    text = strip_tags(text).strip()
    if length != -1 and len(text) > length:
        return "%s ..." % text[:length]

    return text

### for showing the ops log


@register.function
@jinja2.contextfunction
def show_sudolog_feed(context, feed, verbose):
    helper_func = 'sudolog_%s' % feed.op_code
    if helper_func in context:
        return context[helper_func](context, feed, verbose)

    return no_snippet_found(context, feed, verbose)


@register.function
@jinja2.contextfunction
def no_snippet_found(context, feed, verbose):
    request = context['request']
    return jingo.render_to_string(request, 'dashboard/feed_empty.html', {
        'feed': feed, 'verbose': verbose,
    })


@register.function
def make_flavor_option(f, spaces=8):
    if not isinstance(f, dict):
        f = vars(f)
    f['spaces'] = ' ' * (spaces - len(f['name']))

    for i in f.keys():
        if 'size' in i:
            f[i] = digit_mega_formater(f[i])

    c = u'%(name)s: %(spaces)s%(vcpu_count)d核CPU, %(vmem_size)4s内存, 30G系统盘,' \
        u' %(disk_size)4s数据磁盘' % f
    c = c.replace(' ', '&nbsp;')
    c = re.sub(r'\b0MB', '0GB', c)
    v = u'''<option value='%s'>%s</option>''' % (f['id'], c)
    return v


def bk(size, digits, offset=1):

    bytes = size * 1024 * 1024.0
    unit = ''
    if bytes < 1024:
        unit = 'B'
    elif bytes > 1024:
        unit = 'K'
        bytes /= 1024.0

    if unit:
        formatter = '%%.0%df' % digits
        s = formatter % bytes
        s = re.sub(r'\.?0*$', '', s)
        s = '%s%s' % (s, unit)
        return s


@register.function
def digit_mega_formater(size, digits=1, offset=1):
    'convert M to G'
    unit_names = ['M', 'G', 'T', 'P', 'E', 'Z', 'Y']
    u = {}
    size = float(size)
    size /= (1.0 * offset)

    if size < 1.0:
        return bk(size, digits, offset)

    for i in range(len(unit_names)):
        u[unit_names[i]] = 1024 ** i

    index = 0
    while size >= 1024 and index < len(unit_names) - 1:
        size /= 1024.0
        index += 1

    formatter = '%%.0%df' % digits
    s = formatter % size
    s = re.sub(r'\.?0*$', '', s)
    s = '%s%s' % (s, unit_names[index])
    s = re.sub(r'(?i)\b0M\b', '0G', s)
    return s


@register.function
def digit_comma_formater(digit):
    """convert 1111 to 1,111
    http://regex.info/dlisting.cgi?ed=3&id=36405
    """
    regex = re.compile(r'(?<=\d)(?=(\d\d\d)+(?!\d))')
    return regex.sub(',', str(digit))


@register.function
def div_offset(size, offset=1):
    ret = float(size) / (offset * 1.0)
    return float_formatter(ret)


@register.function
def float_formatter(x, digits=2):
    x = float(x)
    formatter = '%%.0%df' % digits
    ret = formatter % x
    ret = re.sub(r'\.?0*$', '', ret)
    return ret


@register.function
def deep_dict(d, *keylist):

    if not isinstance(d, dict):
        d = vars(d)
    for key in keylist[0]:
        try:
            d = d[key]
        except Exception as e:
            logging.error(e)
            d = 0
            break
    return d
