from django.db import models
from lib.utils import lg, wn, er
from pprint import pprint
import time


class Base(models.Model):

    id = models.IntegerField(primary_key=True, blank=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    #@classmethod
    def fields(self):
        return [k for k in vars(self).keys() if not k.startswith('_')]

    def to_dict(self):
        dt = self.__dict__.copy()
        ret = {}
        for k in dt.keys():
            if not k.startswith('_'):
                v = dt[k]
                if not type(v) in [basestring, str, unicode, int, float]:
                    v = (r'%s' % v).replace('+00:00', '')
                ret[k] = v
        return ret

    @classmethod
    def query(cls, limit=100, **query):
        """
        return 0 or more result
        """
        hostname = query.get('hostname')
        if hostname:
            del query['hostname']
            host = Hosts.exact(name=hostname)
            if host:
                query['host_id'] = host.id

        ret = cls.objects.filter(**query).order_by('-id')[:limit] or []

        return [r.to_dict() for r in ret]

    @classmethod
    def exact(cls, **query):
        """
        get only one result, if any
        """
        return cls.objects.get(**query)

    def set(self, **kwargs):
        """
        update the host entry itself;
        """
        hostname = kwargs.get('hostname')
        if not hostname:
            raise "No hostname given"
            return

        if not re.match('^[\w-.]+$', hostname):
            raise "Host name not valid"
            return

        hosts = Hosts.objects.filter(name__exact=hostname)
        if not hosts:
            host = Hosts(name=hostname)
        else:
            host = hosts[0]

        for field in self.fields():
            value = kwargs.get(field)
            if value not in ['None', 'none', None]:
                setattr(host, field, value)

        host.save()
        return host


class Stats(Base):

    host_id = models.IntegerField(blank=True)
    time = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        abstract = True

    @classmethod
    def set(cls, **kwargs):
        """
        update or create an entry that related to host, not the host entry it self.
        """

        item = cls()

        hostname = kwargs.get('hostname')
        if not hostname:
            raise "No hostname given"

        if not re.match('^[\w-.]+$', hostname):
            raise "Host name not valid"
            return

        host, created = Hosts.objects.get_or_create(name=hostname)
        item.host_id = host.id

        for field in item.fields():
            value = kwargs.get(field)
            if value not in ['None', 'none', None]:
                setattr(item, field, value)

        item.save()
        return item


class Hosts(Base):

    name = models.CharField(max_length=192, blank=True, unique=True)
    desc = models.CharField(max_length=768, blank=True)
    ip = models.CharField(max_length=16, blank=True)
    cpu = models.IntegerField(null=True, blank=True)
    disk = models.IntegerField(null=True, blank=True)
    io = models.IntegerField(null=True, blank=True)
    uptime = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = u'hosts'

    def to_dict(self):
        d = h = m = 0
        try:
            self.uptime = int(self.uptime or 0)
        except:
            self.uptime = 0

        d = self.uptime / 86400
        rest = self.uptime % 86400
        h = rest / 3600
        rest %= 3600
        m = rest / 60

        kwargs = {
            'host_id': self.id,
            'limit': 1,
        }

        load = Load.query(**kwargs)
        io = IO.query(**kwargs)

        if load:
            load = load[0]
            # print 'load dict'
            # pprint(load, indent=2)
            self.cpu = [load.get('load_1'), load.get('load_5'), load.get('load_15')]
            self.cpu = ', '.join([str(i) for i in self.cpu])

        if io:
            io = io[0]
            self.io = ', '.join([str(i) for i in [
                io.get('r_s', ''),
                io.get('w_s', ''),
                io.get('kr_s', ''),
                io.get('kw_s', ''),
                io.get('wait', ''),
                io.get('svc_t', ''),
                io.get('pct_b', ''),
            ]])

        stamp = int(time.time())

        color = ''
        if not self.modified:
            color = 'error'
        else:
            sec = int(self.modified.strftime('%s'))
            diff = stamp - sec
            if diff > 86400:
                color = 'error'
            elif diff > 3600 * 12:
                color = 'warning'

        return {
            'name': self.name or '',
            'desc': self.desc or '',
            'ip': self.ip or '',
            'cpu': self.cpu or '',
            'disk': self.disk or '',
            'io': self.io or '',
            'uptime': "%s Days %02d:%02d" % (d,h,m),
            'modified': self.modified,
            'color': color,
        }


class Mem(Stats):

    active = models.BigIntegerField(null=True, blank=True)
    inact = models.BigIntegerField(null=True, blank=True)
    wired = models.BigIntegerField(null=True, blank=True)
    cache = models.BigIntegerField(null=True, blank=True)
    buf = models.BigIntegerField(null=True, blank=True)
    free = models.BigIntegerField(null=True, blank=True)


class Process(Stats):

    total = models.IntegerField(null=True, blank=True)
    run = models.IntegerField(null=True, blank=True)
    slp = models.IntegerField(null=True, blank=True)


class Load(Stats):

    load_1 = models.FloatField(null=True, blank=True)
    load_5 = models.FloatField(null=True, blank=True)
    load_15 = models.FloatField(null=True, blank=True)


class IO(Stats):

    r_s = models.FloatField(null=True, blank=True)
    w_s = models.FloatField(null=True, blank=True)
    kr_s = models.FloatField(null=True, blank=True)
    kw_s = models.FloatField(null=True, blank=True)
    wait = models.IntegerField(null=True, blank=True)
    svc_t = models.FloatField(null=True, blank=True)
    pct_b = models.IntegerField(null=True, blank=True)


class Net(Stats):

    """ rx means downloaded and tx means uploaded """
    in_bits_per_s = models.IntegerField(null=True, blank=True)
    in_pks_per_s = models.IntegerField(null=True, blank=True)
    out_bits_per_s = models.IntegerField(null=True, blank=True)
    out_pks_per_s = models.IntegerField(null=True, blank=True)
