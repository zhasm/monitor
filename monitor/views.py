from django.views.decorators.http import require_http_methods
from lib.utils import lg, wn, er

import jingo
import json
from apps.stats.models import Load

from apps.stats.models import Hosts, Mem, IO, Net

hosts = Hosts().query()

@require_http_methods(['GET'])
def home(request):

    hosts = Hosts().query()

    mem = Mem().query(limit=30, host_id=Hosts.objects.all()[0].id)
    return jingo.render(request, 'home.html', {
        'hosts': hosts,
        'mem': json.dumps(mem),
    })


@require_http_methods(['GET'])
def empty(request):

    return jingo.render(request, 'graph.html', {
        'hosts': hosts,
    })


@require_http_methods(['GET'])
def node(request, hostname):

    cpu = Load.query(hostname=hostname)
    mem = Mem.query(hostname=hostname)
    io = IO.query(hostname=hostname)
    net = Net.query(hostname=hostname)

    keys = lambda x: json.dumps(sorted(set(x().fields()) -
                                       set(['id', 'time', 'host_id'])))

    return jingo.render(request, 'graph.html', {
        "hostname": hostname,
        'hosts': hosts,
        'cpu': json.dumps(cpu),
        'cpu_keys': keys(Load),
        'mem': json.dumps(mem),
        'mem_keys': keys(Mem),
        'io': json.dumps(io),
        'io_keys': keys(IO),
        'net': json.dumps(net),
        'net_keys': keys(Net),
    })
