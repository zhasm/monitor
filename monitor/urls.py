from django.conf.urls import patterns, include, url
from django.conf import settings
import views

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'monitor.views.home', name='home'),
    # url(r'^monitor/', include('monitor.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^/?$', views.home, name='home'),
    url(r'^node/?$', views.empty, name='empty'),
    url(r'^node/(?P<hostname>[^/]+)/?$', views.node, name='node'),
    # url(r'^media/font/(?P<font>.*(?:eot|ttf|oft))$',
    #    views.font, name='font'),
    # url(r'^media/js/(?P<jsfile>.*(?:js))(?:\?v=\w+)?$',
    #    views.js, name='js'),
    url(r'^favicon\.ico$', 'django.views.generic.simple.redirect_to',
       {'url': '/media/img/sigma.ico'}),
    # url(r'^robots\.txt$',
    #    lambda r: HttpResponse("User-agent: *\n"
    #                           "Disallow: /dashboard/\n"
    #                           "Disallow: /users/\n"
    #                           "Disallow: /media/", mimetype="text/plain")),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
       {'document_root': settings.STATIC_ROOT}),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
       {'document_root': settings.MEDIA_ROOT, 'show_indexes': False}),

)
